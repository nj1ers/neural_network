import numpy
import math


class Network:

    # layout = [2,3,2] means 2 neurons in input, 3 in hidden,2 in output
    # this would have 2 weight matrices: a 3x2 -> 1st and 2nd layer and a 2x3 -> 2nd and 3rd
    # two biases (input has no bias): a 3x1 and a 2x1
    def __init__(self, layout):
        self.n = len(layout)
        # generate a list of 2d arrays of the required sizes, with random values
        # number of rows = number of neurons in i, number of columns = neurons in i -1 where i is the layer
        self.weights = [numpy.random.rand(y, x) for x, y in zip(layout[0:-1], layout[1:])]
        # generate bias, no bias for input
        self.biases = [numpy.random.rand(x, 1) for x in layout[1:]]

    # could look nicer but alright for just debugging
    def __str__(self):
        return str(self.weights) + '\n\n' + str(self.biases) + '\n'

    @staticmethod
    def sigmoid(x):
        return 1 / (1 + numpy.exp(-x))

    # we can use matrix multiplication!
    def feed_forward(self, out):
        for w, b in zip(self.weights, self.biases):
            out = Network.sigmoid(numpy.dot(w, out) + b)
        return out


if __name__ == "__main__":

    net = Network([2, 3, 4])
    print(net)
    # use these numpy matrices otherwise it gets all weird
    inp = numpy.matrix('1;2')
    print(net.feed_forward(inp))
